# README #

Pre-requisites : 1. Jira project TST is available and open in browser

Assumptions : 
1. User is logged in with sufficient rights to view and log issues in the "A Test Project" (TST) project.
2. Most negative testing will be done at a later stage as no additional information on fields or requirements were supplied.
3. Database checkpoint will be done at a later stage as I have no access to the DB.