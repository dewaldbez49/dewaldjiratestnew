import junit.framework.Test;
import junit.framework.TestSuite;

public class DewaldJiraTest {

  public static Test suite() {
    TestSuite suite = new TestSuite();
    suite.addTestSuite(CreateNewIssue.class);
    suite.addTestSuite(EditExistingIssue.class);
    suite.addTestSuite(SearchExisting.class);
    return suite;
  }

  public static void main(String[] args) {
    junit.textui.TestRunner.run(suite());
  }
}
