package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateNewIssue {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://jira.atlassian.com/browse/TST";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testCreateNewIssue() throws Exception {
    driver.get(baseUrl + "/browse/TST/?selectedTab=com.atlassian.jira.jira-projects-plugin:issues-panel");
    driver.findElement(By.id("create_link")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [isEditable | id=summary | ]]
    driver.findElement(By.id("create-issue-submit")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [isEditable | id=summary | ]]
    // Warning: assertTextPresent may require manual changes
    assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*You must specify a summary of the issue\\.[\\s\\S]*$"));
    driver.findElement(By.id("summary")).clear();
    driver.findElement(By.id("summary")).sendKeys("This is a bug logging test");
    driver.findElement(By.xpath("//div[@id='components-multi-select']/span")).click();
    driver.findElement(By.cssSelector("a.aui-list-item-link")).click();
    driver.findElement(By.xpath("//div[@id='versions-multi-select']/span")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'A version')])[2]")).click();
    driver.findElement(By.xpath("//div[@id='fixVersions-multi-select']/span")).click();
    driver.findElement(By.xpath("(//a[contains(text(),'Test Version 1')])[3]")).click();
    driver.findElement(By.xpath("//div[@id='assignee-single-select']/span")).click();
    driver.findElement(By.cssSelector("a.aui-list-item-link.aui-iconised-link")).click();
    driver.findElement(By.id("environment")).clear();
    driver.findElement(By.id("environment")).sendKeys("Test Environment");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("This is a test script to create a new Jira Issue");
    driver.findElement(By.id("timetracking")).clear();
    driver.findElement(By.id("timetracking")).sendKeys("15m");
    driver.findElement(By.id("customfield_10653")).clear();
    driver.findElement(By.id("customfield_10653")).sendKeys("3");
    driver.findElement(By.id("create-issue-submit")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
